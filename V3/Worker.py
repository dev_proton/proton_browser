import traceback
import socket
import time
import psutil


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

class Worker(QObject):

	valprogress= pyqtSignal(int)
	listdevice= pyqtSignal(list)
    
	def __init__(self):
		QObject.__init__(self)
		self.List=[]
		self.DeviceList=[]     
        
	def readNetwokAdpater(self):
		all_nic=psutil.net_if_addrs()
		print(psutil.net_if_stats())
		print(all_nic)
		for k, v in all_nic.items():			
			for data in v:
				try :
					if ( (data.family == 2 ) and ( psutil.net_if_stats().get(k).isup == True )):
						datav = ( k, (data.address) )                
						self.DeviceList.append(datav)					                    
				except :
					traceback.print_exc()
		#print("Nember Network Adapter : " + str(len(self.DeviceList)) )
		print(self.DeviceList)

	def browserAll(self):
		self.readNetwokAdpater()
		count=0
		countPool=0
		countInterface=0
		maxAdapters=len(self.DeviceList)
		message = b'\x00\x01\x00'		
		self.List.clear()
		self.valprogress.emit(1)  
		for Adapter in (self.DeviceList):
			print("ip interface: " + str (Adapter) )
			try :
				server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
				server.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
				server.bind((Adapter[1],18778))
				server.settimeout(3)
				flagPool=True
			except :
				print("An exception server occurred")
				traceback.print_exc() 
				flagPool=False                
			if (countInterface == 0) :
				self.valprogress.emit( 10 ) 
			else: 
				self.valprogress.emit( ( (countInterface/maxAdapters)*100 ) )   
			countInterface=countInterface+1			
			countEco=0
			count=0         

			while  ( flagPool==True ) : #
  
				while True :

					try :
						server.sendto(message, ('<broadcast>', 18778))
					except:
						print("An exception send occurred")
						traceback.print_exc()

					time.sleep(0.5)
					try :
						data, addr = server.recvfrom(1024)                   
					except:
						print("An exception recive occurred")
						traceback.print_exc()
						flagPool=False
						break
					m = 0
					print("Received info: "+str(data) + " len : " + str( len (str(data) ) ))
                
					if( len (str(data) ) < 25):
						print("not valid data  --- " + str ( countEco ) ) 
						if ( countEco > 0 ) :
							flagPool=False
							break
						countEco=countEco + 1                       
						break 
                    
					for i in data:
					#print("Received info: "+str(data))
					#print(str(m) + " = " + 	" " + chr(i))			
						if (i==17 and tmp==5):		
							name = m
							break				
						tmp=i
						m = m + 1
					databack=dict()
					databack2=dict()


					if count == 0 :
 							databack2["ip"] = Adapter[1]
 							databack2["name"] = Adapter[0]
 							self.List.append(databack2)                            
					databack["Nadapter"] = Adapter[0]
                    
					print("IP   = " + str(addr[0]))
					print("Network MASK = " + str(data[11]) + ":" + str(data[12]) + ":" + str(data[13]) + ":"  + str(data[14]) )
					print("Network Adapter = " + data[name+20:name+24].decode('utf-8' ))
					print("Name = " + data[29:name-1].decode('utf-8'))
					print("MAC  = " + data[name+1:name+18].decode('utf-8'))
					print("FIRM = " + data[name+38:len(data)].decode('utf-8'))
					print("Temporary IP = " + str(data[17]) + "." + str(data[18]) + "." + str(data[19]) + "."  + str(data[20])  )                
					databack["ip"] = str(addr[0])
					databack["name"] = data[29:name-1].decode('utf-8')
					databack["mac"] = data[name+1:name+18].decode('utf-8')
					databack["firm"] = data[name+38:len(data)].decode('utf-8')
					databack["hostip"] = Adapter[1]
					databack["Nmask"] = str(data[11]) + ":" + str(data[12]) + ":" + str(data[13]) + ":"  + str(data[14])
					databack["Tempip"] = str(data[17]) + "." + str(data[18]) + "." + str(data[19]) + "."  + str(data[20])
					databack["NadapterD"] = data[name+20:name+24].decode('utf-8')
					if ( count > 0 ):
						countPool=0                
						for i in self.List: 
							#print("IP 1 : " + i.get('ip'))

							if( ( i.get('mac') == databack["mac"] ) and  (i.get('NadapterD') == databack["NadapterD"] ) ):  
								print("mac equal")
								flagPool=False							                            
								break 
							countPool=countPool+1                           
						if( countPool == len(self.List) ):
							self.List.append(databack)                           
					else :
						self.List.append(databack)
						#self.List.append(databack)
					count=count+1
					countEco=0          
			server.close()               
		self.valprogress.emit(100)
		self.listdevice.emit(self.List)
