; example2.nsi
;
; This script is based on example1.nsi, but it remember the directory, 
; has uninstall support and (optionally) installs start menu shortcuts.
;
; It will install example2.nsi into a directory that the user selects,

;--------------------------------

; The name of the installer
Name "ProtonBrowser_installer"

; The file to write
OutFile "ProtonBrowser.exe"

; The default installation directory
InstallDir $PROGRAMFILES\Proton

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\Proton" "Install_Dir"

; Request application privileges for Windows Vista
RequestExecutionLevel admin

;--------------------------------

; Pages

Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "Proton (required)"

  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Put file there
  File "ProtonBrowser.exe"
  File  /r "C:\Users\andres.vargas\Desktop\protonbrowser\V3\assets"
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\Proton "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Proton" "DisplayName" "Proton  Browser"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Proton" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Proton" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Proton" "NoRepair" 1
  WriteUninstaller "$INSTDIR\uninstall.exe"
  
SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\Proton"
  CreateShortcut "$SMPROGRAMS\Proton\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
  CreateShortcut "$SMPROGRAMS\Proton\ProtonBrowser .lnk" "$INSTDIR\ProtonBrowser.exe" "" "$INSTDIR\ProtonBrowser.exe" 0
  
SectionEnd


Section "Desktop Shortcut" 

    SetShellVarContext current
    CreateShortCut "$DESKTOP\ProtonBrowser .lnk" "$INSTDIR\ProtonBrowser.exe"
SectionEnd


;--------------------------------

; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Proton"
  DeleteRegKey HKLM SOFTWARE\Proton
  

  ; Remove files and uninstaller
  Delete $INSTDIR\ProtonBrowser.exe
  Delete $INSTDIR\uninstall.exe
 
  
  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\Proton\*.*"
  Delete "$DESKTOP\ProtonBrowser .lnk"
  
  ; Remove directories used
  RMDir   /R /REBOOTOK "$SMPROGRAMS\Proton"
  RMDir  /R /REBOOTOK "$INSTDIR"
  RMDir  /R /REBOOTOK "$INSTDIR\assets"


SectionEnd
