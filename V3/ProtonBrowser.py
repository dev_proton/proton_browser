import traceback, sys
import socket
import subprocess
import webbrowser


from PyQt5 import QtCore, QtGui, QtWidgets,uic
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from Worker import *


# Translate asset paths to useable format for PyInstaller
def resource_path(relative_path):
  if hasattr(sys, '_MEIPASS'):
      return os.path.join(sys._MEIPASS, relative_path)
  return os.path.join(os.path.abspath('.'), relative_path)

class ImageWidget(QtWidgets.QWidget):

	def __init__(self, imagePath, parent):
		super(ImageWidget, self).__init__(parent)
		self.picture = QtGui.QPixmap(imagePath)
		self.picture = self.picture.scaled(50, 90, QtCore.Qt.KeepAspectRatio)
	def paintEvent(self, event):
		painter = QtGui.QPainter(self)
		painter.drawPixmap(20,10, self.picture)


class MainWindow(QtWidgets.QMainWindow ):
	def __init__(self):
		super(MainWindow, self).__init__()
		uic.loadUi('./assets/Browser.ui',self)
 


		self.Afind=self.findChild( QtWidgets.QAction , 'actionBrowser')
		self.Areset=self.findChild( QtWidgets.QAction , 'actionclear')
		self.Aabout=self.findChild( QtWidgets.QAction , 'actionAbout')
		self.Afind.triggered.connect(self.findproton)
		self.Areset.triggered.connect(self.clearTable)
		self.Aabout.triggered.connect(self.aboutInfo)        
	


		self.tableWidget = self.findChild( QtWidgets.QTableWidget , 'tableWidget')

		self.tableWidget.horizontalHeader().setVisible(False)


		self.tableWidget.setSpan(self.tableWidget.rowCount()-1,0,1,2)  
		self.tableWidget.setItem(self.tableWidget.rowCount()-1,0,QTableWidgetItem(str("Module Name")))
		item = self.tableWidget.item(self.tableWidget.rowCount()-1, 0)         
		item.setTextAlignment( Qt.AlignCenter )
		item.setBackground(QtGui.QColor(27,45,107))   
		item.setForeground(QtGui.QColor(255,255,255)) 
		item.setFlags( QtCore.Qt.ItemIsSelectable |  QtCore.Qt.ItemIsEnabled )

		self.tableWidget.setItem(self.tableWidget.rowCount()-1,2,QTableWidgetItem(str("MAC")))
		item = self.tableWidget.item(self.tableWidget.rowCount()-1, 2)         
		item.setTextAlignment( Qt.AlignCenter )
		item.setBackground(QtGui.QColor(27,45,107))   
		item.setForeground(QtGui.QColor(255,255,255)) 
		item.setFlags( QtCore.Qt.ItemIsSelectable |  QtCore.Qt.ItemIsEnabled )

		self.tableWidget.setItem(self.tableWidget.rowCount()-1,3,QTableWidgetItem(str("IP Address")))
		item = self.tableWidget.item(self.tableWidget.rowCount()-1, 3)         
		item.setTextAlignment( Qt.AlignCenter )
		item.setBackground(QtGui.QColor(27,45,107))   
		item.setForeground(QtGui.QColor(255,255,255)) 
		item.setFlags( QtCore.Qt.ItemIsSelectable |  QtCore.Qt.ItemIsEnabled )

		self.tableWidget.setItem(self.tableWidget.rowCount()-1,4,QTableWidgetItem(str("Temporary IP")))
		item = self.tableWidget.item(self.tableWidget.rowCount()-1, 4)         
		item.setTextAlignment( Qt.AlignCenter )
		item.setBackground(QtGui.QColor(27,45,107))   
		item.setForeground(QtGui.QColor(255,255,255)) 
		item.setFlags( QtCore.Qt.ItemIsSelectable |  QtCore.Qt.ItemIsEnabled )

		self.tableWidget.setItem(self.tableWidget.rowCount()-1,5,QTableWidgetItem(str("Firmware Version")))
		item = self.tableWidget.item(self.tableWidget.rowCount()-1, 5)         
		item.setTextAlignment( Qt.AlignCenter )
		item.setBackground(QtGui.QColor(27,45,107))   
		item.setForeground(QtGui.QColor(255,255,255)) 
		item.setFlags( QtCore.Qt.ItemIsSelectable |  QtCore.Qt.ItemIsEnabled )

		self.tableWidget.insertRow(self.tableWidget.rowCount()) 

		self.tableWidget.setFocusPolicy(Qt.NoFocus)
		self.tableWidget.doubleClicked.connect(self.on_clickTable)
		self.tableWidget.setColumnWidth(0, 60)
		self.tableWidget.setColumnWidth(1, 110)
		self.tableWidget.setColumnWidth(2, 110)
		self.tableWidget.setColumnWidth(3, 80)
		self.tableWidget.setColumnWidth(4, 100)
		self.tableWidget.setColumnWidth(5, 130)
		self.tableWidget.setColumnHidden(6, True)
		self.tableWidget.setColumnHidden(7, True)		
		self.tableWidget.setColumnHidden(8, True)
		self.tableWidget.setColumnHidden(9, True)
        

		header = self.tableWidget.horizontalHeader()       
    
		header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
		header.setSectionResizeMode(2, QtWidgets.QHeaderView.Stretch)	
		header.setSectionResizeMode(3, QtWidgets.QHeaderView.Stretch)
		header.setSectionResizeMode(4, QtWidgets.QHeaderView.Stretch)	        
		header.setSectionResizeMode(5, QtWidgets.QHeaderView.Stretch)
        
		item=self.tableWidget.horizontalHeaderItem(1)       
		self.tableWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)     # +++
        ### This signal is emitted when the widget's contextMenuPolicy is Qt::CustomContextMenu, 
        ### and the user has requested a context menu on the widget. 
		self.tableWidget.customContextMenuRequested.connect(self.generateMenu) # +++

		self.tableWidget.viewport().installEventFilter(self)     

        
		self.progress_bar = QtWidgets.QProgressDialog("Searching PROTONS....", None, 0, 100, self.centralwidget)
		self.progress_bar.setWindowTitle("SCAN")
		self.progress_bar.setWindowModality(Qt.WindowModal) 
		self.progress_bar.close()  

	def aboutInfo(self):
		'''msg = QMessageBox()
		msg.setIcon(QMessageBox.Warning)
		msg.setText("Proton IoT was created in 2004 and since then we have been creating customs solutions for our customers. Our main goal is to satisfy the customer requirements and expectations and at the same time to create the best working environment possible. We have been involved in multiple types of projects, developing hardware and software for small and big systems alike, always keeping a cost consciousness mind, and having the highest standards in customer service. For Proton our Mission Statement is not just a marketing fad but is what guides everything we do everyday. That has been the secret of our success and what pushes us to be better everyday.")
		msg.setWindowTitle("PROTON selection")
		msg.setWindowIcon(QtGui.QIcon('logo-icon.jpg'))					
		retval = msg.exec_()'''                
		dialog = Dialog_Info(self) 
		dialog.show()  


        
	def eventFilter(self, source, event):
		if(event.type() == QtCore.QEvent.MouseButtonPress and
			event.buttons() == QtCore.Qt.RightButton and
			source is self.tableWidget.viewport()):
			item = self.tableWidget.itemAt(event.pos())
			print('Global Pos:', event.globalPos())
			if item is not None:
			    print('Table Item:', item.row(), item.column())
			    self.menu = QMenu(self)
			    if  int((item.row())) > 1 :
			        self.menu.addAction("Config Temporary IP",self.tempIP)      
			        self.menu.addAction("Delete Temporary IP",self.tempIP_Remove) 
		return super(MainWindow, self).eventFilter(source, event)

    ### +++    
	def generateMenu(self, pos):
		print("pos======",pos)
		self.menu.exec_(self.tableWidget.mapToGlobal(pos))   # +++

	def tempIP(self):
		listTemp=[] 
        
		for currentQTableWidgetItem in self.tableWidget.selectedItems():
			print(currentQTableWidgetItem.row(), currentQTableWidgetItem.column(), currentQTableWidgetItem.text())
			print(self.tableWidget.item(currentQTableWidgetItem.row(),1).text())
			listTemp.append(self.tableWidget.item(currentQTableWidgetItem.row(),1).text())
			print(self.tableWidget.item(currentQTableWidgetItem.row(),2).text())
			listTemp.append(self.tableWidget.item(currentQTableWidgetItem.row(),2).text())
			print(self.tableWidget.item(currentQTableWidgetItem.row(),3).text())
			listTemp.append(self.tableWidget.item(currentQTableWidgetItem.row(),3).text())
			print(self.tableWidget.item(currentQTableWidgetItem.row(),6).text())
			listTemp.append(self.tableWidget.item(currentQTableWidgetItem.row(),6).text())
			print(self.tableWidget.item(currentQTableWidgetItem.row(),7).text())               
			listTemp.append(self.tableWidget.item(currentQTableWidgetItem.row(),7).text())
			print(self.tableWidget.item(currentQTableWidgetItem.row(),8).text())               
			listTemp.append(self.tableWidget.item(currentQTableWidgetItem.row(),8).text())
			print(self.tableWidget.item(currentQTableWidgetItem.row(),9).text())               
			listTemp.append(self.tableWidget.item(currentQTableWidgetItem.row(),9).text())
		dialog = Dialog_TempIP(self) 
		dialog.set_values(listTemp)
		dialog.endwork.connect(self.refeshAll)
		#self.listdataT.emit(set_values)
		dialog.show()

	def tempIP_Remove(self):
		listTemp=[] 
        
		for currentQTableWidgetItem in self.tableWidget.selectedItems():
			print(currentQTableWidgetItem.row(), currentQTableWidgetItem.column(), currentQTableWidgetItem.text())
			print(self.tableWidget.item(currentQTableWidgetItem.row(),1).text())
			listTemp.append(self.tableWidget.item(currentQTableWidgetItem.row(),1).text())
			print(self.tableWidget.item(currentQTableWidgetItem.row(),2).text())
			listTemp.append(self.tableWidget.item(currentQTableWidgetItem.row(),2).text())
			print(self.tableWidget.item(currentQTableWidgetItem.row(),3).text())
			listTemp.append(self.tableWidget.item(currentQTableWidgetItem.row(),3).text())
			print(self.tableWidget.item(currentQTableWidgetItem.row(),6).text())
			listTemp.append(self.tableWidget.item(currentQTableWidgetItem.row(),6).text())
			print(self.tableWidget.item(currentQTableWidgetItem.row(),7).text())               
			listTemp.append(self.tableWidget.item(currentQTableWidgetItem.row(),7).text())
			print(self.tableWidget.item(currentQTableWidgetItem.row(),8).text())               
			listTemp.append(self.tableWidget.item(currentQTableWidgetItem.row(),8).text())
			print(self.tableWidget.item(currentQTableWidgetItem.row(),9).text())               
			listTemp.append(self.tableWidget.item(currentQTableWidgetItem.row(),9).text())
		dialog = Dialog_TempIP(self) 
		dialog.set_values(listTemp)
		dialog.endwork.connect(self.refeshAll)
		#self.listdataT.emit(set_values)
		dialog.remove_Tempip()



	def msgbtn (self ,i) :

		if(i.text() in "Config") :
			print ("Button pressed is oki "  )
			self.tempIP()

	@pyqtSlot()   
	def on_clickTable(self):
    
    
		tempUrl=""
		for currentQTableWidgetItem in self.tableWidget.selectedItems():
			#print(currentQTableWidgetItem.row(), currentQTableWidgetItem.column(), currentQTableWidgetItem.text())    
			if currentQTableWidgetItem.row() < 2 :
				print(" invalid row ")
				return
			hostip=self.tableWidget.item(currentQTableWidgetItem.row(),8) .text()
			#print("data "+ hostip +  " len : " + str(len(hostip)) )
			if len( hostip ) > 1 :
				ip=self.tableWidget.item(currentQTableWidgetItem.row(),3).text()
				Nmask=self.tableWidget.item(currentQTableWidgetItem.row(),6).text()
				tempip=self.tableWidget.item(currentQTableWidgetItem.row(),4).text()
				if (( self.IpinAddress( hostip ,ip , Nmask ) )):
					tempUrl="http://" + ip
					webbrowser.open(tempUrl)
				elif ( tempip not in "0.0.0.0") :
					print("temp ip config")                
					tempUrl="http://" + tempip
					webbrowser.open(tempUrl)
				else :

					msg = QMessageBox()
					msg.setIcon(QMessageBox.Warning)
					msg.setText("this device cannot be accessed either by the primary IP. Please assing a temporary IP Address in the same subnet as this pc to continue")
					msg.setWindowTitle("PROTON selection")
					msg.setWindowIcon(QtGui.QIcon('./assets/icon.png'))					
					msg.setStandardButtons(QMessageBox.Yes| QMessageBox.No)
					buttonY = msg.button(QMessageBox.Yes)
					buttonY.setText('Config')
					buttonN = msg.button(QMessageBox.No)
					buttonN.setText('Cancel')
					msg.buttonClicked.connect(self.msgbtn)

					'''
					msg.setDetailedText("The details are as follows:")
					msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
					msg.buttonClicked.connect(msgbtn)
					'''
					retval = msg.exec_()

					#QMessageBox.about(self, "PROTON selection", "this device cannot be accessed either by the primary IP. Please assing a temporary IP Address in the same subnet as this pc to continue")
		#self.tempIP() 
    

	def findproton(self):	
		print(" find_ proton")          
		self.clearTable()
		#subprocess.run(["netsh", "interface", "set", "interface", self.Sadapter, "ENABLED"])	
		self.progress_bar.show()
		# Pass the function to execute
		worker = Worker() # Any other args, kwargs are passed to the run function
		#worker.signals.connect(self.progress_fn)'
		# Execute
		worker.valprogress.connect(self.progress_fn)
		worker.listdevice.connect(self.browser_complete)
		worker.browserAll()

	def progress_fn(self, value):

		self.progress_bar.setValue(value)
		#print(value)
   

	def clearTable(self):


		while (self.tableWidget.rowCount() > 1) :
			self.tableWidget.removeRow(1)
		self.tableWidget.insertRow(self.tableWidget.rowCount())
		self.tableWidget.setItem(self.tableWidget.rowCount()-1,0,QtWidgets.QTableWidgetItem(('')));
		self.tableWidget.setItem(self.tableWidget.rowCount()-1,1,QtWidgets.QTableWidgetItem(('')));
		self.tableWidget.setItem(self.tableWidget.rowCount()-1,2,QtWidgets.QTableWidgetItem(('')));
		self.tableWidget.setItem(self.tableWidget.rowCount()-1,3,QtWidgets.QTableWidgetItem(('')));
		self.tableWidget.setItem(self.tableWidget.rowCount()-1,4,QtWidgets.QTableWidgetItem(('')));
		self.tableWidget.setItem(self.tableWidget.rowCount()-1,5,QtWidgets.QTableWidgetItem(('')));

        
	def refeshAll(self):

		self.clearTable()
		self.findproton()
		print("End dialog")

	def print_output(self, s):
		print(s)

	def ip_to_binary(self , ip):
		octet_list_int = ip.split(".")
		octet_list_bin = [format(int(i), '08b') for i in octet_list_int]
		binary = ("").join(octet_list_bin)
		return binary

	def get_addr_network(self,address, net_size):
		#Convert ip address to 32 bit binary
		ip_bin = self.ip_to_binary(address)
		#Extract Network ID from 32 binary
		network = ip_bin[0:32-(32-net_size)]    
		return network


	def IpinAddress (self , ip1 ,ip2 ,ip2_mask):

		ip2_mask= sum(bin(int(x)).count('1') for x in ip2_mask.split(':'))
		ip2= ip2 + "/" + str( ip2_mask )                
		#print("Ip1 : " + ip1 + "  Ip 2 : " + ip2 )
		#CIDR based separation of address and network size
		[prefix_address, net_size] = ip2.split("/")
		#Convert string to int
		net_size = int(net_size)
		#Get the network ID of both prefix and ip based net size
		prefix_network = self.get_addr_network(prefix_address, net_size)
		ip_network = self.get_addr_network(ip1, net_size)
		if( ip_network == prefix_network ) :
 			return True        
        
		return False






	def browser_complete(self ,s ):
		count=0
		print("THREAD COMPLETE!")
		self.progress_bar.close() 
     
		for i in s:   
			

			if (i.get('Nadapter')is  None ) : 
				font = QFont()
				font.setBold(True)
				font.setFamily("HelveticaNeue")
				font.setPointSize(10) 
				self.tableWidget.setSpan(self.tableWidget.rowCount()-1,0,1,6)                
				self.tableWidget.setItem(self.tableWidget.rowCount()-1,0,QtWidgets.QTableWidgetItem(" Network Adapter : " + i.get('name') + " --- " + i.get('ip')))			         
				self.tableWidget.item(self.tableWidget.rowCount()-1,0).setForeground(QtGui.QColor(27,45,107))            
				self.tableWidget.item(self.tableWidget.rowCount()-1,0).setFont(font)
				#self.tableWidget.verticalHeader().setDefaultSectionSize(300)
				self.tableWidget.setRowHeight((self.tableWidget.rowCount()-1) , 30)
				self.tableWidget.item(self.tableWidget.rowCount()-1, 0)         
				#item.setTextAlignment( Qt.AlignCenter )                
                
                
				
                #self.tableWidget.item(self.tableWidget.rowCount()-1,1).setBackground(QtGui.QColor(0,0,125))
			else : 
				#self.tableWidget.verticalHeader().setDefaultSectionSize(40)           
				self.tableWidget.setCellWidget(self.tableWidget.rowCount()-1,0,ImageWidget("./assets/Proton_CMB_Frontal.jpg",self))				
				self.tableWidget.setRowHeight((self.tableWidget.rowCount()-1) ,110)
				self.tableWidget.setItem(self.tableWidget.rowCount()-1,1,QtWidgets.QTableWidgetItem(i.get('name')))
				item = self.tableWidget.item(self.tableWidget.rowCount()-1, 1)         
				item.setTextAlignment( Qt.AlignCenter )
				item.setForeground(QtGui.QColor(27,45,107))             
				self.tableWidget.setItem(self.tableWidget.rowCount()-1,2,QtWidgets.QTableWidgetItem(i.get('mac')))
				item = self.tableWidget.item(self.tableWidget.rowCount()-1, 2)         
				item.setTextAlignment( Qt.AlignCenter )
				item.setForeground(QtGui.QColor(27,45,107))                 
				self.tableWidget.setItem(self.tableWidget.rowCount()-1,3,QtWidgets.QTableWidgetItem(i.get('ip')))
				item = self.tableWidget.item(self.tableWidget.rowCount()-1, 3)         
				item.setTextAlignment( Qt.AlignCenter )
				item.setForeground(QtGui.QColor(27,45,107)) 
				self.tableWidget.setItem(self.tableWidget.rowCount()-1,4,QtWidgets.QTableWidgetItem(i.get('Tempip')))
				item = self.tableWidget.item(self.tableWidget.rowCount()-1, 4)         
				item.setForeground(QtGui.QColor(27,45,107)) 
				item.setTextAlignment( Qt.AlignCenter )
				self.tableWidget.setItem(self.tableWidget.rowCount()-1,5,QtWidgets.QTableWidgetItem(i.get('firm')))
				item = self.tableWidget.item(self.tableWidget.rowCount()-1, 5)  
				item.setTextAlignment( Qt.AlignCenter )	
				item.setForeground(QtGui.QColor(27,45,107)) 
				self.tableWidget.setItem(self.tableWidget.rowCount()-1,6,QtWidgets.QTableWidgetItem(i.get('Nmask')))
				self.tableWidget.setItem(self.tableWidget.rowCount()-1,7,QtWidgets.QTableWidgetItem(i.get('Nadapter')))
				self.tableWidget.setItem(self.tableWidget.rowCount()-1,8,QtWidgets.QTableWidgetItem(i.get('hostip')))            
				self.tableWidget.setItem(self.tableWidget.rowCount()-1,9,QtWidgets.QTableWidgetItem(i.get('NadapterD')))			
			#print(i.get('Nadapter'))
			#print(i.get('Nmask'))

			if (i.get('Nadapter')is not None ) :
				
				font = QFont()
				font.setBold(True)
				if( self.IpinAddress( i.get('hostip') , i.get('ip') , i.get('Nmask')  ) ) :
					self.tableWidget.item(self.tableWidget.rowCount()-1,3).setForeground(QtGui.QColor(0,125,0))
					self.tableWidget.item(self.tableWidget.rowCount()-1,3).setFont(font)               
				else :
					self.tableWidget.item(self.tableWidget.rowCount()-1,3).setForeground(QtGui.QColor(125,0,0))
					self.tableWidget.item(self.tableWidget.rowCount()-1,3).setFont(font)

			self.tableWidget.insertRow(self.tableWidget.rowCount())         
			count=count+1
		self.tableWidget.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
		#self.tableWidget.resizeColumnsToContents()

class Dialog_TempIP(QDialog):

	endwork= pyqtSignal()

	def __init__(self, *args, **kwargs):
		super(Dialog_TempIP, self).__init__(*args, **kwargs)
        
		self.setWindowTitle("Temporary IP")
		self.Sadapter='wEthernet'        
		self.groupbox = QGroupBox("Grid")
		self.groupbox.setCheckable(True)
        
		QBtn = QDialogButtonBox.Ok | QDialogButtonBox.Cancel
 

		self.buttonBox = QDialogButtonBox(QBtn)
		self.buttonBox.button(QDialogButtonBox.Ok).setText('Set')
		self.buttonBox.button(QDialogButtonBox.Cancel).setText('Delete')
		self.buttonBox.accepted.connect(self.set_Tempip)
		self.buttonBox.rejected.connect(self.remove_Tempip)
        
 		#self.setStyleSheet("QDialog {background: \'white\';}")
 
		self.text1 = QtWidgets.QLabel(self)
		self.text1.setObjectName("text")
		self.text1.setText("Device Name :",)

		self.text2 = QtWidgets.QLabel(self)
		self.text2.setObjectName("text")
		self.text2.setText("sdjfsdkjf")

		self.text3 = QtWidgets.QLabel(self)
		self.text3.setObjectName("text")
		self.text3.setText("Device IP addrees :",)

		self.text4 = QtWidgets.QLabel(self)
		self.text4.setObjectName("text")
		self.text4.setText("0.0.0.0",)

		self.text5 = QtWidgets.QLabel(self)
		self.text5.setObjectName("text")
		self.text5.setText("Serila Number :",)

		self.text6 = QtWidgets.QLabel(self)
		self.text6.setObjectName("text")
		self.text6.setText("12345678",)

		self.text7 = QtWidgets.QLabel(self)
		self.text7.setObjectName("text")
		self.text7.setText("Network Mask :",)

		self.text8 = QtWidgets.QLabel(self)
		self.text8.setObjectName("text")
		self.text8.setText("0.0.0.0",)

		self.text9 = QtWidgets.QLabel(self)
		self.text9.setObjectName("text")
		self.text9.setText("Temporary IP :",)

		self.text10 = QtWidgets.QLineEdit(self)
		self.text10.setInputMask("000.000.000.000")
		self.text10.setObjectName("text")
		self.text10.setText("0.0.0.0",)

		self.text11 = QtWidgets.QLabel(self)
		self.text11.setObjectName("text")
		self.text11.setText("Temporary Network Mask :",)

		self.text12 = QtWidgets.QLineEdit(self)
		self.text12.setInputMask("000.000.000.000")
		self.text12.setObjectName("text")
		self.text12.setText("255.255.0.0",)
        
		self.textDadap = ""
		self.textNadap = ""
		self.textIphost = ""

		self.layout = QGridLayout()
		self.groupbox.setLayout(self.layout)
		self.layout.addWidget(self.text1,0,0)
		self.layout.addWidget(self.text2,0,1)
		self.layout.addWidget(self.text3,1,0)
		self.layout.addWidget(self.text4,1,1)
		self.layout.addWidget(self.text5,2,0)
		self.layout.addWidget(self.text6,2,1)
		self.layout.addWidget(self.text7,3,0)
		self.layout.addWidget(self.text8,3,1)
		self.layout.addWidget(self.text9,4,0)
		self.layout.addWidget(self.text10,4,1)
		self.layout.addWidget(self.text11,5,0)
		self.layout.addWidget(self.text12,5,1)        
		self.layout.addWidget(self.buttonBox,6,1)
		self.setLayout(self.layout)

	def set_values(self, s):
		print(s)        
		self.text2.setText( s[0] )		
		self.text4.setText( s[2] )  
		self.text6.setText( s[1] )
		self.text8.setText( s[3] )
		self.textNadap =( s[4] )
		self.textIphost = ( s[5]  )    
		self.textDadap =( s[6] )


	def set_Tempip(self):       
		print("Ok Button") 
		server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
		server.bind((self.textIphost,18778))
		server.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
		# Set a timeout so the socket does not block
		# indefinitely when trying to receive data.
		#server.bind(("", 18778))                                      00:         60:        E9:          15:        C8:       FE        tempID      172 30 3 200                255  255 255 0
		#message = b'\x00\x01\x02\x07\x04\x65\x74\x68\x30\x05\x11\x30\x30\x3a\x36\x30\x3a\x45\x39\x3a\x31\x35\x3a\x43\x38\x3a\x46\x45\x01\x04\xac\x1e\x03\xc8\x02\x04\xff\xff\xff\x00'
		mac=self.text6.text()
		iptemp=self.text10.text()#"172.30.3.200"
		masktemp=self.text12.text() #"255.255.255.0"
		parts_ip=iptemp.split('.')#
		parts_mask=masktemp.split('.')
		#s = hex(int( parts_ip[0]))

		ip_hex=(int(parts_ip[0]).to_bytes(1, byteorder='big')) + (int(parts_ip[1]).to_bytes(1, byteorder='big')) + (int(parts_ip[2]).to_bytes(1, byteorder='big')) + (int(parts_ip[3]).to_bytes(1, byteorder='big'))
		#ip_hex=ip_hex.replace("0x","\x")
		print(str(ip_hex))
		mask_hex=(int(parts_mask[0]).to_bytes(1, byteorder='big')) + (int(parts_mask[1]).to_bytes(1, byteorder='big')) + (int(parts_mask[2]).to_bytes(1, byteorder='big')) + (int(parts_mask[3]).to_bytes(1, byteorder='big'))
		print(str(mask_hex))
		message = b'\x00\x01\x02\x07\x04' + bytes( self.textDadap,"utf-8")  + b'\x05\x11'+ bytes( mac ,"utf-8") + b'\x01\x04' + ip_hex  +b'\x02\x04' + mask_hex
		#10.0.50.100'<broadcast>' 
       

		try :
			server.sendto(message, ('<broadcast>', 18778))
			print("send data " + str(message))
		except:
			print("An exception send occurred")
			traceback.print_exc()
		server.close()
		time.sleep(0.75)         
		self.endwork.emit()
		self.close()        


	def remove_Tempip(self):       
		mac=self.text6.text()
		print("Ok Button") 
		server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
		server.bind((self.textIphost,18778))
		server.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
		message = b'\x00\x01\x03\x07\x04'+ bytes( self.textDadap,"utf-8") +b'\x05\x11' + bytes( mac ,"utf-8") 

		try :
			server.sendto(message, ('<broadcast>', 18778))

			#data, addr = server.recvfrom(1024)
			print("send remove " + str(message))
		except:
			print("An exception send occurred")
			traceback.print_exc()

		server.close()
		time.sleep(0.75)        
		self.endwork.emit()
		self.close()

class Dialog_Info(QDialog):

	

	def __init__(self, *args, **kwargs):
		super(Dialog_Info, self).__init__(*args, **kwargs)
		uic.loadUi('./assets/info.ui', self) 
		self.Lversion=self.findChild( QtWidgets.QLabel , 'label_version')
		self.Lversion.setText("version: 3.0")
		self.show()        
		'''
		self.setWindowTitle("Develpers")        
		self.groupbox = QGroupBox("Grid")
		self.groupbox.setCheckable(True)

		self.textinfo = QtWidgets.QLabel(self)
		self.textinfo.setObjectName("textinfo")
		self.textinfo.setText("Proton IoT was created in 2004 and since then we have been creating customs solutions for our customers. Our main goal is to satisfy the customer requirements and expectations and at the same time to create the best working environment possible. We have been involved in multiple types of projects, developing hardware and software for small and big systems alike, always keeping a cost consciousness mind, and having the highest standards in customer service. For Proton our Mission Statement is not just a marketing fad but is what guides everything we do everyday. That has been the secret of our success and what pushes us to be better everyday.")

		self.textWeb = QtWidgets.QLabel(self)
		self.textWeb.setObjectName("textweb")
		self.textWeb.setText("<a href=\"http://www.qtcentre.org\">QtCentre</a>")
		
		self.layout = QGridLayout()
		self.groupbox.setLayout(self.layout)
		self.layout.addWidget(self.textinfo,0,0)
		self.layout.addWidget(self.textWeb,1,0)
		self.setLayout(self.layout)
		'''
if __name__ == "__main__":
	app = QtWidgets.QApplication(sys.argv)
    
    
	window = MainWindow()
	window.show()
	#window.findproton()
	sys.exit(app.exec_())